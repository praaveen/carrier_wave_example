class YourScaffoldsController < ApplicationController
  # GET /your_scaffolds
  # GET /your_scaffolds.json
  def index
    @your_scaffolds = YourScaffold.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @your_scaffolds }
    end
  end

  # GET /your_scaffolds/1
  # GET /your_scaffolds/1.json
  def show
    @your_scaffold = YourScaffold.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @your_scaffold }
    end
  end

  # GET /your_scaffolds/new
  # GET /your_scaffolds/new.json
  def new
    @your_scaffold = YourScaffold.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @your_scaffold }
    end
  end

  # GET /your_scaffolds/1/edit
  def edit
    @your_scaffold = YourScaffold.find(params[:id])
  end

  # POST /your_scaffolds
  # POST /your_scaffolds.json
  def create
    @your_scaffold = YourScaffold.new(params[:your_scaffold])

    respond_to do |format|
      if @your_scaffold.save
        format.html { redirect_to @your_scaffold, notice: 'Your scaffold was successfully created.' }
        format.json { render json: @your_scaffold, status: :created, location: @your_scaffold }
      else
        format.html { render action: "new" }
        format.json { render json: @your_scaffold.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /your_scaffolds/1
  # PUT /your_scaffolds/1.json
  def update
    @your_scaffold = YourScaffold.find(params[:id])

    respond_to do |format|
      if @your_scaffold.update_attributes(params[:your_scaffold])
        format.html { redirect_to @your_scaffold, notice: 'Your scaffold was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @your_scaffold.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /your_scaffolds/1
  # DELETE /your_scaffolds/1.json
  def destroy
    @your_scaffold = YourScaffold.find(params[:id])
    @your_scaffold.destroy

    respond_to do |format|
      format.html { redirect_to your_scaffolds_url }
      format.json { head :no_content }
    end
  end
end
